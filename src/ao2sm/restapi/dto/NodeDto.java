package ao2sm.restapi.dto;

import java.util.*;

import ao2sm.restapi.dto.accessibility.IElementBase;
import ao2sm.restapi.dto.accessibility.amenity.parking;
import ao2sm.restapi.dto.accessibility.amenity.pharmacy;
import ao2sm.restapi.dto.accessibility.highway.BusStop;
import ao2sm.restapi.dto.accessibility.highway.Crossing;
import ao2sm.restapi.dto.accessibility.highway.TrafficSignals;
import ao2sm.restapi.dto.accessibility.highway.TramStop;
import ao2sm.restapi.dto.accessibility.railway.Station;
import ao2sm.restapi.dto.*;

public class NodeDto {
	private int id;
	private String user;
	private String uid;
	private String changeset;
	private double lon;
	private double lat;
	private String version;
	private Boolean visible;
	private String timestamp;
	private TagsDtoList Tags = new TagsDtoList();



	public NodeDto(){
		
	} 

	public String getChangeset() {
		return changeset;
	}
	public void setChangeset(String changeset) {
		this.changeset = changeset;
	}
	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public TagsDtoList getTags() {
		return Tags;
	}																											
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public Boolean getVisible() {
		return visible;
	}
	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	private Boolean isAmenity(){
		return this.Tags.ContainsAmenity();
	}
	
	private Boolean isHighway(){
		return this.Tags.ContainsHighway();
	}
	
	private String getAmenityType(){
		return this.Tags.getAmenityValue();
	}
	
	private Boolean isRailway(){
		return this.Tags.ContainsKey("railway");
	}
	
	private String getName(){
		return this.Tags.getTagValue("name");
	}
	
	public boolean IsToBeMapped(){
		if(!isAmenity() && !isHighway())
			return false;
		
		if(isAmenity()){
			String amenityname = getAmenityType().toLowerCase();
			ArrayList<String> validAmenities = new ArrayList<String>();
			
			validAmenities.add("parking");
			validAmenities.add("pharmacy");
			validAmenities.add("toilet");
			return validAmenities.contains(amenityname);
		}
		
		if(isHighway()){
			String amenityname = this.Tags.getTagValue("highway").toLowerCase();
			ArrayList<String> validHighways = new ArrayList<String>();
			
			validHighways.add("bus_stop");
			validHighways.add("crossing");
			validHighways.add("traffic_signals");
			validHighways.add("tram_stop");
			return validHighways.contains(amenityname);
		}
		
		if(isRailway()){
			String railwayname = this.Tags.getTagValue("railway").toLowerCase();
			ArrayList<String> validRailWays = new ArrayList<String>();
			validRailWays.add("station");
			validRailWays.add("subway_entrance");
			
			return validRailWays.contains(railwayname);
		}
		return false;
	}
	 	
	public IElementBase GetElementBase(){
		if(isAmenity()){
			switch(getAmenityType().toLowerCase()){
				case "parking":
				 return GetElementParking();
				case "pharmacy":
					return GetElementPharmacy();
			}
		}
		if(isHighway()){
			switch (this.Tags.getTagValue("highway").toLowerCase()) {
			case "bus_stop":
					return GetElementHighwayBusStop();
			case "crossing":
					return GetElementCrossing();
			case "traffic_signals":
				TrafficSignals t = new TrafficSignals();
				t.setNode(this);
				return t;
			case "tram_stop":
				TramStop tramstop = new TramStop();
				tramstop.setNode(this);
				return tramstop;
			}
		}
		
		if(isRailway()){
			switch (this.Tags.getTagValue("railway").toLowerCase()) {
			case "station":
			case "subway_entrance":	 
				Station st = new Station();
				 st.setNode(this);
				 return st;
				

			default:
				break;
			}
		}
		
		return null;
	}

	private IElementBase GetElementCrossing() {
		Crossing c = new Crossing();
		c.setName(this.getName());
		c.setCrossing(this.getTags().getTagValue("crossing"));
		c.setElementid(this.getId());
		c.setCrossing_Ref(this.getTags().getTagValue("crossing_ref"));
		c.setSloped_Curb(this.getTags().getTagValue("sloped_curb"));
		c.setNode(this);
		c.setTactile_Paving(this.getTags().getTagValue("tactile_paving"));
		
		return c;
	}

	private IElementBase GetElementPharmacy() {
	  pharmacy p = new pharmacy();
	  p.setName(this.getName());
	  p.setElementid(this.getId());
	  p.setWheelchair(this.Tags.getTagValue("wheelchair"));
	  return p;
	}

	private IElementBase GetElementParking() {
		parking p = new parking();
		p.setElementid(this.getId());
		p.setName(this.Tags.getTagValue("name"));
		p.setSpacesForDisabled(this.Tags.getTagValueInt("accessibility:spacesForDisabled"));
		return p;
	}

	private IElementBase GetElementHighwayBusStop(){
		BusStop b = new BusStop();
		b.setElementid(this.getId());
		b.setName(this.getName());
		b.setShelter(this.getTags().getTagValue("shelter").toLowerCase().equals("yes"));
		b.setTactilepaving(this.getTags().getTagValue("tactile_paving").toLowerCase().equals("yes"));
		b.setWheelchair(this.getTags().getTagValue("wheelchair"));
		return b;
	}
}
