package ao2sm.restapi.dto;

import java.util.*;

public class MapRequestDto {
	private NodeDtoList Nodes = new NodeDtoList();
	private List<WayDto> Ways = new ArrayList<WayDto>();
	
	
	private BoundsDto bounds ;
	
	
	public String GetGeoJSON(){
		String strJson = "";
		
		strJson += "{";
		strJson +="\"type\":\"FeatureCollection\",";
		strJson +="\"features\":[";
		
		for(int i=0; i< this.Nodes.size(); i++){
			NodeDto node = this.Nodes.get(i);
			if(!node.getVisible())
				continue;
			
			if(node.getTags().size()==0)
				continue;
			
			strJson += "{";
			strJson += " \"type\":\"Feature\",";
			strJson += " \"geometry\":{\"type\":\"Point\",\"coordinates\":["+ node.getLon() + ", "+ node.getLat() +"]},";
			strJson += " \"properties\": {";
			strJson += " \"id\":\"" + node.getId() +"\",";
			strJson += " \"tags\":[";
			
			int tagsLength = node.getTags().size();
			for(int itag = 0; itag < tagsLength; itag++){
				TagDto t = node.getTags().get(itag);
				strJson += "{\"k\":\"" + t.getKey() + "\",";
				strJson += "\"v\":\"" + t.getValue() + "\"}";
				if(itag + 1 < tagsLength)
					strJson += ",";
			}

			strJson +="]";
			
			strJson += ",\"type\":\"node\"";
			
			strJson += "}"; //fim das properties
			strJson += "}";
			if(i+1<this.Nodes.size())
				strJson+=",";
		}
		
	//	if(strJson.endsWith(","))
		//	strJson = strJson.substring(0, strJson.length() - 1);
		
		
		
		
		
		for(int i=0; i<this.Ways.size(); i++){
		
			WayDto way = this.Ways.get(i);
			if(!way.isVisible())
				continue;
			
			if(way.getTags().size()==0)
				continue;
			
			String stype = "LineString";
			NodeDto first = way.getNodes().get(0);
			NodeDto last = way.getNodes().get(way.getNodes().size()-1);
			if(first!=null && last != null){
				if(first.getLon() == last.getLon() && first.getLat() == last.getLat())
					stype = "Polygon";
			}
			
		 	if(stype.equals("Polygon"))
		 		continue;
			
			strJson += "{";
			strJson += " \"type\":\"Feature\",";
			strJson += " \"geometry\":{\"type\":\"";
			
			
			strJson +=stype;
			
			strJson +="\",\"coordinates\":[";
			for(int c =0;c<way.getNodes().size();c++){
				NodeDto no = way.getNodes().get(c);
				strJson += "[" + no.getLon() + ", " + no.getLat() + "]";
				if(c+1<way.getNodes().size())
					strJson +=",";
			}
			
			strJson += "]},";
			strJson += " \"properties\": {";
			strJson += " \"id\":\"" + way.getId() +"\",";
			strJson += " \"tags\":[";
			
			int tagsLength = way.getTags().size();
			for(int itag = 0; itag < tagsLength; itag++){
				TagDto t = way.getTags().get(itag);
				strJson += "{\"k\":\"" + t.getKey() + "\",";
				strJson += "\"v\":\"" + t.getValue() + "\"}";
				if(itag + 1 < tagsLength)
					strJson += ",";
			}

			
			strJson +="]";

			strJson += ",\"type\":\"way\"";
			
			strJson += "}"; //fim das properties
			strJson += "}";
			if(i+1<this.Ways.size())
				strJson +=",";
		}
		
		if(strJson.endsWith(","))
			strJson = strJson.substring(0, strJson.length() - 1);
		
		
		
		
		strJson +="]}";		
		
		return strJson;
	}
	
	
	 


	public List<NodeDto> getNodes() {
		return Nodes;
	}
	
	public BoundsDto getBounds() {
		return bounds;
	}
	public void setBounds(BoundsDto bounds) {
		this.bounds = bounds;
	}
	public List<WayDto> getWays() {
		return Ways;
	}
}
