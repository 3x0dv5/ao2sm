package ao2sm.restapi.dto;

import java.util.*;


public class WayDto {
	private int id;
	private boolean visible;
	private String timestamp;
	private String version;
	private int changeset;
	private String user;
	private int uid;
	
	private List<TagDto> Tags = new ArrayList<TagDto>();
 
	private List<NdDto> Nds = new ArrayList<NdDto>();
	
	private NodeDtoList Nodes = new NodeDtoList();
	 
	

	public WayDto(){
		
	}
	
	public boolean isVisible() {
		return visible;
	}
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getChangeset() {
		return changeset;
	}
	public void setChangeset(int changeset) {
		this.changeset = changeset;
	}
	public List<TagDto> getTags() {
		return Tags;
	}
	public List<NdDto> getNds() {
		return Nds;
	}
	public NodeDtoList getNodes() {
		return Nodes;
	}

	public String GetHTML() {
		return "\"<b>this is html data</b>\"";
	}
}

