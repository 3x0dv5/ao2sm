package ao2sm.restapi.dto;

import com.thoughtworks.xstream.XStream;

public class WayMapper {
	public XStream GetMap(String xmlToMap){
		XStream xs = new XStream();
		
		xs.alias("osm", WayRootDto.class);
		xs.alias("way", WayDto.class);		
		xs.alias("nd", NdDto.class);
		xs.alias("tag", TagDto.class);
		
	 	if(xmlToMap.contains("<nd"))
			xs.addImplicitCollection(WayDto.class,"Nds", NdDto.class);
	 
		if(xmlToMap.contains("<tag"))
			xs.addImplicitCollection(WayDto.class, "Tags", TagDto.class);
	 
		
		xs.useAttributeFor(WayDto.class, "id");
		xs.useAttributeFor(WayDto.class, "visible");
		xs.useAttributeFor(WayDto.class, "timestamp");
		xs.useAttributeFor(WayDto.class, "version");
		xs.useAttributeFor(WayDto.class, "changeset");
		xs.useAttributeFor(WayDto.class, "user");
		xs.useAttributeFor(WayDto.class, "uid");
		
	 	xs.useAttributeFor(NdDto.class,"ref");
	 	
		xs.useAttributeFor(TagDto.class,"Key");
		xs.useAttributeFor(TagDto.class,"Value");
		xs.aliasField("k", TagDto.class, "Key");
		xs.aliasField("v", TagDto.class, "Value");
		
		
		return xs;
	}
}
