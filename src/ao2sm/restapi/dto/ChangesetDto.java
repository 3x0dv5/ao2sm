package ao2sm.restapi.dto;

import java.util.*;

public class ChangesetDto {

	private List<TagDto> Tags = new ArrayList<TagDto>();
	private int id;
	private String user;
	private int uid;
	private String createdAt;
	private String closedAt;
	private boolean open;
	private double minLon;
	private double minLat;
	private double maxLon;
	private double maxLat;
	 
	
	public ChangesetDto(){}
	
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getUser() {
		return user;
	}


	public void setUser(String user) {
		this.user = user;
	}


	public int getUid() {
		return uid;
	}


	public void setUid(int uid) {
		this.uid = uid;
	}


	public String getCreated_at() {
		return createdAt;
	}


	public void setCreated_at(String created_at) {
		this.createdAt = created_at;
	}

	public String getClosed_at() {
		return closedAt;
	}


	public void setClosed_at(String closed_at) {
		this.closedAt = closed_at;
	}

	public boolean isOpen() {
		return open;
	}


	public void setOpen(boolean open) {
		this.open = open;
	}


	public double getMin_lon() {
		return minLon;
	}


	public void setMin_lon(double min_lon) {
		this.minLon = min_lon;
	}


	public double getMin_lat() {
		return minLat;
	}


	public void setMin_lat(double min_lat) {
		this.minLat = min_lat;
	}


	public double getMax_lon() {
		return maxLon;
	}


	public void setMax_lon(double max_lon) {
		this.maxLon = max_lon;
	}


	public double getMax_lat() {
		return maxLat;
	}


	public void setMax_lat(double max_lat) {
		this.maxLat = max_lat;
	}


	 


	
	public List<TagDto> getTags() {
		return Tags;
	}
	
}
