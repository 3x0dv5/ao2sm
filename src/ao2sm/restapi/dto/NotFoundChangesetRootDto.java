package ao2sm.restapi.dto;




public class NotFoundChangesetRootDto extends ChangesetRootDto {
	
	public NotFoundChangesetRootDto(){
		ChangesetDto notFound = new ChangesetDto();
		notFound.setId(-1);
		this.setChangeset(notFound);	
	}
	
	
}
