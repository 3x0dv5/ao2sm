package ao2sm.restapi.dto;

public class  NotFoundNodeRootDto extends NodeRootDto {

	public NotFoundNodeRootDto(){
		NodeDto d = new NodeDto();
		d.setId(-1);
		this.setNode(d);
	}
}
