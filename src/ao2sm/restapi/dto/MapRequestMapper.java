package ao2sm.restapi.dto;




import java.io.IOException;
import java.io.StringReader;
import java.util.*;

import com.thoughtworks.xstream.*;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException; 
import org.w3c.dom.*;

public class MapRequestMapper {
	
	public XStream GetMap(String xmlToMap){
		//XStream xs = new XStream();
				
				XStream xs = new XStream() ;
		 		xs.alias("osm", MapRequestDto.class);
				xs.alias("node", NodeDto.class);	
				xs.alias("bounds", BoundsDto.class);
				
				if(xmlToMap.contains("<node"))
					xs.addImplicitCollection(MapRequestDto.class, "Nodes");
				
				xs.useAttributeFor(NodeDto.class, "id");
				xs.useAttributeFor(NodeDto.class, "user");
				xs.useAttributeFor(NodeDto.class, "uid");
				xs.useAttributeFor(NodeDto.class, "changeset");
				xs.useAttributeFor(NodeDto.class, "lon");
				xs.useAttributeFor(NodeDto.class, "lat");
				xs.useAttributeFor(NodeDto.class, "version");
				xs.useAttributeFor(NodeDto.class, "visible");
				xs.useAttributeFor(NodeDto.class, "timestamp");
				
				xs.useAttributeFor(BoundsDto.class, "minlat");
				xs.useAttributeFor(BoundsDto.class, "minlon");
				xs.useAttributeFor(BoundsDto.class, "maxlat");
				xs.useAttributeFor(BoundsDto.class, "maxlon");
				
				
				/*xs.alias("tag", TagDto.class);
				if(xmlToMap.contains("<tag "));
					xs.addImplicitCollection(NodeDto.class, "Tags");
				
				xs.useAttributeFor(NodeDto.class, "id");
				xs.useAttributeFor(NodeDto.class, "user");
				xs.useAttributeFor(NodeDto.class, "uid");
				
				xs.useAttributeFor(NodeDto.class, "changeset");
				xs.useAttributeFor(NodeDto.class, "lon");
				xs.useAttributeFor(NodeDto.class, "lat");
				xs.useAttributeFor(NodeDto.class, "version");
				xs.useAttributeFor(NodeDto.class, "visible");
				xs.useAttributeFor(NodeDto.class, "timestamp");
				
				xs.useAttributeFor(TagDto.class,"Key");
				xs.useAttributeFor(TagDto.class,"Value");
				xs.aliasField("k", TagDto.class, "Key");
				xs.aliasField("v", TagDto.class, "Value");
				*/
				return xs;
				

			}
	
	
	
	public MapRequestDto fromXML(String xml){
		
		MapRequestDto mapDto = new MapRequestDto();
		 
		
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(xml));
			Document doc = docBuilder.parse(is);
			
			mapDto.setBounds(getBounds(doc));
			
			NodeDtoList nodes =  getNodes(doc);
			
			for(NodeDto n: nodes){
				if(n.IsToBeMapped()){
					mapDto.getNodes().add(n);
				}
			}
			
		/*	for(int i = 0; i<nodes.size(); i++){
				NodeDto n = nodes.get(i);
				if(n.IsToBeMapped()){
					mapDto.getNodes().add(n);
				}
			}
			*/
			
			/*
			List<WayDto> ways = getWays(doc, nodes);
			for(int i=0; i<ways.size(); i++){
				mapDto.getWays().add(ways.get(i));
			}
			*/
			
			
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return mapDto;
	}



	private List<WayDto> getWays(Document doc, NodeDtoList nodes) {
		List<WayDto> _ways = new ArrayList<WayDto>();
		
		NodeList waysList = doc.getElementsByTagName("way");
		int waysLength = waysList.getLength();
		
		for(int i = 0; i < waysLength; i++){
			Node wayNode = waysList.item(i);
			
			if(wayNode.getNodeType() == Node.ELEMENT_NODE){
				Element elWay = (Element)wayNode;
				WayDto way = new WayDto();
				way.setId(Integer.parseInt(elWay.getAttribute("id")));
				way.setUser(elWay.getAttribute("user"));
				way.setUid(Integer.parseInt(elWay.getAttribute("uid")));
				way.setVisible(Boolean.parseBoolean(elWay.getAttribute("visible")));
				way.setVersion(elWay.getAttribute("version"));
				way.setChangeset(Integer.parseInt(elWay.getAttribute("changeset")));
				way.setTimestamp(elWay.getAttribute("timestamp"));
				
				NodeList ndNodes = elWay.getElementsByTagName("nd");
				Integer indNodesLength = ndNodes.getLength();
				for(int ind = 0; ind < indNodesLength; ind++){
					Node ndNode = ndNodes.item(ind);
					if(ndNode.getNodeType() == Node.ELEMENT_NODE){
						Element elndNode = (Element) ndNode;
						NodeDto nodeDto = nodes.FindByNodeId(Integer.parseInt(elndNode.getAttribute("ref")));
						if(nodeDto!=null){
							way.getNodes().add(nodeDto);
						}
					}
				}
				
				
				NodeList tagNodes = elWay.getElementsByTagName("tag");
				int tagNodesLength = tagNodes.getLength();
				if(tagNodesLength>0){
					for(int iTag = 0; iTag<tagNodesLength; iTag++){
						Node tagNode = tagNodes.item(iTag);
						if(tagNode.getNodeType() == Node.ELEMENT_NODE){
							Element elTag = (Element)tagNode;
							
							TagDto tag = new TagDto();
							tag.setKey(elTag.getAttribute("k"));
							tag.setValue(elTag.getAttribute("v"));
							way.getTags().add(tag);
						}
					}
				}
				_ways.add(way);
			}
			
		}
		
		return _ways;
	}



	private NodeDtoList getNodes(Document doc) {
		NodeDtoList _nodes = new NodeDtoList();
		
		NodeList nodesList = doc.getElementsByTagName("node");
		
		for(int temp =0; temp <nodesList.getLength(); temp++){
			Node nNode = nodesList.item(temp);
			 
			if(nNode.getNodeType() == Node.ELEMENT_NODE){
				Element el = (Element) nNode;
				
				NodeDto n = new NodeDto();
				
				n.setId(Integer.parseInt(el.getAttribute("id")));
				n.setLat(Double.parseDouble(el.getAttribute("lat")));
				n.setLon(Double.parseDouble(el.getAttribute("lon")));
				n.setUser(el.getAttribute("user"));
				n.setUid(el.getAttribute("uid"));
				n.setVisible(Boolean.parseBoolean(el.getAttribute("visible")));
				n.setVersion(el.getAttribute("version"));
				n.setChangeset(el.getAttribute("changeset"));
				n.setTimestamp(el.getAttribute("timestamp"));
				
				
				NodeList tagNodes = el.getElementsByTagName("tag");
				int tagNodesLength = tagNodes.getLength();
				if(tagNodesLength>0){
					for(int iTag = 0; iTag<tagNodesLength; iTag++){
						Node tagNode = tagNodes.item(iTag);
						if(tagNode.getNodeType() == Node.ELEMENT_NODE){
							Element elTag = (Element)tagNode;
							
							TagDto tag = new TagDto();
							tag.setKey(elTag.getAttribute("k"));
							tag.setValue(elTag.getAttribute("v"));
							n.getTags().add(tag);
						}
					}
				}
				_nodes.add(n);
				
				/*
				n.setId(Integer.parseInt(attributes.getNamedItem("id").getNodeValue()));
				n.setLat(Double.parseDouble(attributes.getNamedItem("lat").getNodeValue()));
				n.setLon(Double.parseDouble(attributes.getNamedItem("lon").getNodeValue()));
				n.setUser(attributes.getNamedItem("user").getNodeValue());
				n.setUid(attributes.getNamedItem("uid").getNodeValue());
				n.setVisible(Boolean.parseBoolean(attributes.getNamedItem("visible").getNodeValue()));
				n.setVersion(attributes.getNamedItem("version").getNodeValue());
				n.setChangeset(attributes.getNamedItem("changeset").getNodeValue());
				n.setTimestamp(attributes.getNamedItem("timestamp").getNodeValue());*/
				
				
			}
			
			
			
		}
		return _nodes;
	}
	
	private BoundsDto getBounds(Document doc){
		BoundsDto _boundsDto = null;
		
		NodeList bounds = doc.getElementsByTagName("bounds");
		
		if(bounds.getLength()>0){
			_boundsDto = new BoundsDto();
			
			Node nBounds = bounds.item(0);
			NamedNodeMap attributes =  nBounds.getAttributes();
			_boundsDto.setMinlat(Double.parseDouble(attributes.getNamedItem("minlat").getNodeValue()));
			_boundsDto.setMinlon(Double.parseDouble(attributes.getNamedItem("minlon").getNodeValue()));
			_boundsDto.setMaxlat(Double.parseDouble(attributes.getNamedItem("maxlat").getNodeValue()));
			_boundsDto.setMaxlon(Double.parseDouble(attributes.getNamedItem("minlon").getNodeValue()));
		}
		return _boundsDto;
	}
}
