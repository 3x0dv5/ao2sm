package ao2sm.restapi.dto;

import com.thoughtworks.xstream.XStream;

public class ChangesetMapper {
	
	public XStream GetMap(String xmlToMap){
		XStream xs = new XStream();
		xs.alias("osm", ChangesetRootDto.class);
		xs.alias("changeset", ChangesetDto.class);
		xs.alias("tag", TagDto.class);

		if(xmlToMap.contains("<tag "))
			xs.addImplicitCollection(ChangesetDto.class, "Tags");
		xs.useAttributeFor(ChangesetDto.class, "id");
		xs.useAttributeFor(ChangesetDto.class, "user");
		xs.useAttributeFor(ChangesetDto.class, "uid");
		xs.useAttributeFor(ChangesetDto.class, "createdAt");
		xs.aliasField("created_at", ChangesetDto.class, "createdAt");
		xs.useAttributeFor(ChangesetDto.class, "closedAt");
		xs.aliasField("closed_at", ChangesetDto.class, "closedAt");
		xs.useAttributeFor(ChangesetDto.class, "open");
		xs.useAttributeFor(ChangesetDto.class, "minLon");
		xs.aliasField("min_lon", ChangesetDto.class, "minLon");
		xs.useAttributeFor(ChangesetDto.class, "minLat");
		xs.aliasField("min_lat", ChangesetDto.class, "minLat");
		xs.useAttributeFor(ChangesetDto.class, "maxLon");
		xs.aliasField("max_lon", ChangesetDto.class, "maxLon");
		xs.useAttributeFor(ChangesetDto.class, "maxLat");
		xs.aliasField("max_lat", ChangesetDto.class, "maxLat");
		
		xs.useAttributeFor(TagDto.class,"Key");
		xs.useAttributeFor(TagDto.class,"Value");
		xs.aliasField("k", TagDto.class, "Key");
		xs.aliasField("v", TagDto.class, "Value");
		return xs;

	}
	
	
}
