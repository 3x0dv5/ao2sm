package ao2sm.restapi.dto;

import com.thoughtworks.xstream.XStream;

public class NodeMapper {
	public XStream GetMap(String xmlToMap){
XStream xs = new XStream();
		
		xs.alias("osm", NodeRootDto.class);
		xs.alias("node", NodeDto.class);		
		xs.alias("tag", TagDto.class);
		if(xmlToMap.contains("<tag "));
			xs.addImplicitCollection(NodeDto.class, "Tags");
		
		xs.useAttributeFor(NodeDto.class, "id");
		xs.useAttributeFor(NodeDto.class, "user");
		xs.useAttributeFor(NodeDto.class, "uid");
		
		xs.useAttributeFor(NodeDto.class, "changeset");
		xs.useAttributeFor(NodeDto.class, "lon");
		xs.useAttributeFor(NodeDto.class, "lat");
		xs.useAttributeFor(NodeDto.class, "version");
		xs.useAttributeFor(NodeDto.class, "visible");
		xs.useAttributeFor(NodeDto.class, "timestamp");
		
		xs.useAttributeFor(TagDto.class,"Key");
		xs.useAttributeFor(TagDto.class,"Value");
		xs.aliasField("k", TagDto.class, "Key");
		xs.aliasField("v", TagDto.class, "Value");
		return xs;
		

	}
	
}
