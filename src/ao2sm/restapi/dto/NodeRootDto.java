package ao2sm.restapi.dto;

public class NodeRootDto {
private NodeDto node;
	

	public NodeRootDto(){
		
	}

	public static NodeRootDto NotFound(){
		return new NotFoundNodeRootDto();
	}
	
	public NodeDto getNode() {
		return node;
	}

	public void setNode(NodeDto node) {
		this.node = node;
	}
}
