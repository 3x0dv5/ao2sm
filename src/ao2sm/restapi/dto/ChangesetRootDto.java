package ao2sm.restapi.dto;

public class ChangesetRootDto {
private ChangesetDto changeset;
	
	public ChangesetDto getChangeset() {
		return changeset;
	}

	public void setChangeset(ChangesetDto changeset) {
		this.changeset = changeset;
	}

	public ChangesetRootDto(){
		
	}
	
	public static ChangesetRootDto NotFound(){
		return new NotFoundChangesetRootDto();
	}
}
