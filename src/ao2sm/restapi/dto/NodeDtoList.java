package ao2sm.restapi.dto;

import java.util.ArrayList;

public class NodeDtoList extends ArrayList<NodeDto>{

	public NodeDto FindByNodeId(int NodeId){
		Integer len = this.size();
		for(int i = 0; i < len; i++){
			if(this.get(i).getId()==NodeId)
				return this.get(i);
		}
		return null;
	}
}
