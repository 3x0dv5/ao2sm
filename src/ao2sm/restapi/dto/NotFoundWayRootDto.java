package ao2sm.restapi.dto;

public class NotFoundWayRootDto extends WayRootDto {
	
	public NotFoundWayRootDto(){
		this.setWay(new WayDto());
		this.getWay().setId(-1);
		 
	}
}
