package ao2sm.restapi.dto;

public class WayRootDto {
private WayDto way;

	
	public static WayRootDto NotFound(){
		return new NotFoundWayRootDto();
	}
	
	public WayDto getWay() {
		return way;
	}

	public void setWay(WayDto way) {
		this.way = way;
	}
}
