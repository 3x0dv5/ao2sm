package ao2sm.restapi.dto.accessibility.highway;

import java.io.IOException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

import ao2sm.restapi.config.OSMConfigurations;
import ao2sm.restapi.dto.NodeDto;
import ao2sm.restapi.dto.accessibility.IElementBase;
import ao2sm.restapi.helpers.JavascriptHelper;
import ao2sm.restapi.web.controllers.NodeController;

public class TrafficSignals implements IElementBase {
	private NodeDto node;

	
	@Override
	public String ToHtmlForm() {
		String filepath = OSMConfigurations.htmlTemplatesPath() +  "highway_traffic_signals.html";
		String html ="";
		String[] parameters = new String[5];
		parameters[0] = "elementid";
		parameters[1] = "vibration_select";
		parameters[2] = "arrow_select";
		parameters[3] = "minimap_select";
		parameters[4] = "sound_select";
		
		
		try {
			html = ao2sm.restapi.helpers.FileHelper.readFile(filepath);
			  
			String [] yesnooptions ={"yes","no"};
			String vibration = this.getNode().getTags().getTagValue("traffic_signals:vibration");
			for(String c: yesnooptions){
				if(vibration.toLowerCase().equalsIgnoreCase(c)){
					html= html.replace("{vibration_" + c.toLowerCase() + "_selected}", "selected=\"selected\"" );
				}else{
					html= html.replace("{vibration_" + c.toLowerCase() + "_selected}", "" );
				}
			}
			
		 
			String arrow = this.getNode().getTags().getTagValue("arrow");
			for(String c: yesnooptions){
				if(arrow.toLowerCase().equalsIgnoreCase(c)){
					html= html.replace("{arrow_" + c.toLowerCase() + "_selected}", "selected=\"selected\"" );
				}else{
					html= html.replace("{arrow_" + c.toLowerCase() + "_selected}", "" );
				}
			}
			
			String minimap = this.getNode().getTags().getTagValue("minimap");
			for(String c: yesnooptions){
				if(minimap.toLowerCase().equalsIgnoreCase(c)){
					html= html.replace("{minimap_" + c.toLowerCase() + "_selected}", "selected=\"selected\"" );
				}else{
					html= html.replace("{minimap_" + c.toLowerCase() + "_selected}", "" );
				}
			}
			
				
			String sound = this.getNode().getTags().getTagValue("sound");
			for(String c: yesnooptions){
				if(sound.toLowerCase().equalsIgnoreCase(c)){
					html= html.replace("{sound_" + c.toLowerCase() + "_selected}", "selected=\"selected\"" );
				}else{
					html= html.replace("{sound_" + c.toLowerCase() + "_selected}", "" );
				}
			}
			
			
			html= html.replace("{elementid}", "" + this.getNode().getId());
			html= html.replace("{_name_}", "" + this.getNode().getTags().getTagValue("name"));
			html = html.replace("{function_ok_clicked}", "" + JavascriptHelper.btnOkClickedScript("/submit/highway/traffic_signals",parameters));
		} catch (IOException e) {
			e.printStackTrace();
			html = e.getMessage();
		}
		return html;
	}

	public String SubmitData(int elementId, String vibration_select,
			String arrow_select, String minimap_select, String sound_select) {
		 
		String xmlchangeset="<osm><changeset><tag k=\"created_by\" v=\"AO2SM 0.0.1\"/>";
		xmlchangeset += "<tag k=\"comment\" v=\"Updating some accessibility information into busstop\"/>";
		xmlchangeset += "</changeset></osm>";
		HTTPBasicAuthFilter authfilter = new HTTPBasicAuthFilter("ao2sm", "qwerty01!");
		Client cli = new Client();
		cli.addFilter(authfilter);  
		
		WebResource res = cli.resource(ao2sm.restapi.config.OSMConfigurations.APIURL() + "changeset/create");
		String xmlChangesetId =  res.put(String.class, xmlchangeset);
		
		NodeDto n = new NodeController().GetNodeFromOSM(elementId);
		n.setUser("ao2sm");
		n.setUid("720593");
		n.setChangeset(xmlChangesetId);
		
		n.getTags().setTagValue("traffic_signals:vibration",vibration_select);
		n.getTags().setTagValue("traffic_signals:arrow",arrow_select);
		n.getTags().setTagValue("traffic_signals:minimap",minimap_select);
		n.getTags().setTagValue("traffic_signals:sound",sound_select);
		
		String version  = "";
		try{
			String nodeXml = new NodeController().GetNodeXML(n);
			WebResource resnode = cli.resource(ao2sm.restapi.config.OSMConfigurations.APIURL() + "node/" + n.getId());
			version =  resnode.put(String.class, nodeXml);	
		}catch(Exception ex){
			String error = ex.getMessage();
			return error;
		}
		res = cli.resource(ao2sm.restapi.config.OSMConfigurations.APIURL() + "changeset/" + xmlChangesetId + "/close");
		xmlChangesetId =  res.put(String.class, xmlchangeset);
		
		return "Dados actualizados";
	}

	public NodeDto getNode() {
		return node;
	}


	public void setNode(NodeDto node) {
		this.node = node;
	}

}
