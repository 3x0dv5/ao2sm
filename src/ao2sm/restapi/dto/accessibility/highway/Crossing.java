package ao2sm.restapi.dto.accessibility.highway;

import java.io.IOException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

import ao2sm.restapi.config.OSMConfigurations;
import ao2sm.restapi.dto.NodeDto;
import ao2sm.restapi.dto.accessibility.IElementBase;
import ao2sm.restapi.helpers.JavascriptHelper;
import ao2sm.restapi.web.controllers.NodeController;

public class Crossing implements IElementBase{
	private int elementid;
	private String name;
	private String crossing ;
	private String Crossing_Ref;
	private String  Sloped_Curb;
	private String Tactile_Paving;
	private NodeDto node;
	
	public int getElementid() {
		return elementid;
	}

	public void setElementid(int elementid) {
		this.elementid = elementid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCrossing() {
		return crossing;
	}

	public void setCrossing(String crossing) {
		this.crossing = crossing;
	}

	public String getCrossing_Ref() {
		return Crossing_Ref;
	}

	public void setCrossing_Ref(String crossing_Ref) {
		Crossing_Ref = crossing_Ref;
	}

	public String getSloped_Curb() {
		return Sloped_Curb;
	}

	public void setSloped_Curb(String sloped_Curb) {
		Sloped_Curb = sloped_Curb;
	}

	public String getTactile_Paving() {
		return Tactile_Paving;
	}

	public void setTactile_Paving(String tactile_Paving) {
		Tactile_Paving = tactile_Paving;
	}
	
	public NodeDto getNode(){
		return node;
	}
	public void setNode(NodeDto node){
		this.node = node; 
	}

	@Override
	public String ToHtmlForm() {
		 
		
		String filepath = OSMConfigurations.htmlTemplatesPath() +  "highway_crossing.html";
		String html ="";
		String[] parameters = new String[9];
		parameters[0] = "elementid";
		parameters[1] = "crossing_unmarked_checked";
		parameters[2] = "crossing_uncontrolled_checked";
		parameters[3] = "crossing_traffic_signals_checked";
		parameters[4] = "crossing_traffic_signals_sound_checked";
		parameters[5] = "crossing_traffic_signals_vibration_checked";
		parameters[6] = "crossing_ref_select";
		parameters[7] = "sloped_curb_select";
		parameters[8] = "tactile_paving_select";
		
		try {
			html = ao2sm.restapi.helpers.FileHelper.readFile(filepath);
			
			String crossing_traffic_signals_sound_checked = this.getNode().getTags().getTagValue("traffic_signals:sound");
			String crossing_traffic_signals_vibration_checked= this.getNode().getTags().getTagValue("traffic_signals:vibration");
			
			
			html= html.replace("{crossing_unmarked_checked}", this.getCrossing().toLowerCase().equals("unmarked")?"checked":"" );
			html= html.replace("{crossing_uncontrolled_checked}", this.getCrossing().toLowerCase().equals("uncontrolled")?"checked":"" );
			html= html.replace("{crossing_traffic_signals_checked}", this.getCrossing().toLowerCase().equals("traffic_signals")?"checked":"" );
			
			html= html.replace("{crossing_traffic_signals_sound_checked}", crossing_traffic_signals_sound_checked.toLowerCase().equals("yes")?"checked":"" );
			html= html.replace("{crossing_traffic_signals_vibration_checked}", crossing_traffic_signals_vibration_checked.toLowerCase().equals("yes")?"checked":"" );
			
			
			String [] crossingRefOptions ={"Zebra","Pelican","Toucan","Pegasus","Unset"};
			String crossing_ref = this.getNode().getTags().getTagValue("crossing_ref");
			for(String c: crossingRefOptions){
				if(crossing_ref.toLowerCase().equalsIgnoreCase(c)){
					html= html.replace("{crossing_ref_" + c.toLowerCase() + "_selected}", "selected=\"selected\"" );
				}else{
					html= html.replace("{crossing_ref_" + c.toLowerCase() + "_selected}", "" );
				}
			}
			
			String[] slopedCurbOptions = {"yes","no","one","both","unset"};
			String sloped_curb = this.getNode().getTags().getTagValue("sloped_curb");
			for(String c: slopedCurbOptions){
				if(sloped_curb.toLowerCase().equalsIgnoreCase(c)){
					html= html.replace("{sloped_curb_" + c.toLowerCase() + "_selected}", "selected=\"selected\"" );
				}else{
					html= html.replace("{sloped_curb_" + c.toLowerCase() + "_selected}", "" );
				}
			}
			
			String[] tactile_paving_selectOptions = {"yes","no","incorrect","unset"};
			String tactile_paving = this.getNode().getTags().getTagValue("tactile_paving");
			for(String c: tactile_paving_selectOptions){
				if(tactile_paving.toLowerCase().equalsIgnoreCase(c)){
					html= html.replace("{tactile_paving_" + c.toLowerCase() + "_selected}", "selected=\"selected\"" );
				}else{
					html= html.replace("{tactile_paving_" + c.toLowerCase() + "_selected}", "" );
				}
			}
						
			
			html= html.replace("{elementid}", "" + this.getElementid());
			html= html.replace("{_name_}", "" + this.getName());
			html = html.replace("{function_ok_clicked}", "" + JavascriptHelper.btnOkClickedScript("/submit/highway/crossing",parameters));
		} catch (IOException e) {
			e.printStackTrace();
			html = e.getMessage();
		}
		return html;
	}
	
	 

	public String SubmitData(int elementId,
			boolean crossing_unmarked_checked,
			boolean crossing_uncontrolled_checked,
			boolean crossing_traffic_signals_checked,
			boolean crossing_traffic_signals_sound_checked,
			boolean crossing_traffic_signals_vibration_checked,
			String crossing_ref, String sloped_curb, String tactile_paving) {
		
		String xmlchangeset="<osm><changeset><tag k=\"created_by\" v=\"AO2SM 0.0.1\"/>";
		xmlchangeset += "<tag k=\"comment\" v=\"Updating some accessibility information into busstop\"/>";
		xmlchangeset += "</changeset></osm>";
		HTTPBasicAuthFilter authfilter = new HTTPBasicAuthFilter("ao2sm", "qwerty01!");
		Client cli = new Client();
		cli.addFilter(authfilter);  
		
		WebResource res = cli.resource(ao2sm.restapi.config.OSMConfigurations.APIURL() + "changeset/create");
		String xmlChangesetId =  res.put(String.class, xmlchangeset);
		
		NodeDto n = new NodeController().GetNodeFromOSM(elementId);
		String crossing = n.getTags().getTagValue("crossing");
		String traffic_signals_sound =n.getTags().getTagValue("traffic_signals:sound");
		String traffic_signals_vibration =n.getTags().getTagValue("traffic_signals:vibration");
		
		if(crossing_unmarked_checked)
			crossing = "unmarked";
		if(crossing_uncontrolled_checked)
			crossing ="uncontrolled";
		if(crossing_traffic_signals_checked)
			crossing = "traffic_signals";
		if(crossing_traffic_signals_vibration_checked)
			traffic_signals_vibration = "yes";
		if(crossing_traffic_signals_sound_checked)
			traffic_signals_sound = "yes";
		
		
		n.setUser("ao2sm");
		n.setUid("720593");
		n.setChangeset(xmlChangesetId);
		
		n.getTags().setTagValue("crossing",crossing);
		if(traffic_signals_sound.equals("yes"))
			n.getTags().setTagValue("traffic_signals:sound", "yes" );
		if(traffic_signals_vibration.equals("yes"))
			n.getTags().setTagValue("traffic_signals:vibration", "yes" );
			
		
			
		n.getTags().setTagValue("crossing_ref", crossing_ref);
		n.getTags().setTagValue("sloped_curb", sloped_curb);
		n.getTags().setTagValue("tactile_paving", tactile_paving);
		
		String version  = "";
		try{
			String nodeXml = new NodeController().GetNodeXML(n);
			WebResource resnode = cli.resource(ao2sm.restapi.config.OSMConfigurations.APIURL() + "node/" + n.getId());
			version =  resnode.put(String.class, nodeXml);	
		}catch(Exception ex){
			String error = ex.getMessage();
			return error;
		}
		res = cli.resource(ao2sm.restapi.config.OSMConfigurations.APIURL() + "changeset/" + xmlChangesetId + "/close");
		xmlChangesetId =  res.put(String.class, xmlchangeset);
		
		return "Dados actualizados";
	}

	
	

}
