package ao2sm.restapi.dto.accessibility.railway;
import java.io.IOException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

import ao2sm.restapi.config.OSMConfigurations;
import ao2sm.restapi.dto.NodeDto;
import ao2sm.restapi.dto.accessibility.IElementBase;
import ao2sm.restapi.helpers.JavascriptHelper;
import ao2sm.restapi.web.controllers.NodeController;

public class Station implements IElementBase{
private NodeDto node;

	
	@Override
	public String ToHtmlForm() {
		String filepath = OSMConfigurations.htmlTemplatesPath() +  "railway_station.html";
		String html ="";
		String[] parameters = new String[2];
		parameters[0] = "elementid";
		parameters[1] = "wheelchair_select";
		
		try {
			html = ao2sm.restapi.helpers.FileHelper.readFile(filepath);
			  
			String [] yesnooptions ={"yes","no","limited"};
			String wheelchair = this.getNode().getTags().getTagValue("wheelchair");
			for(String c: yesnooptions){
				if(wheelchair.toLowerCase().equalsIgnoreCase(c)){
					html= html.replace("{wheelchair_" + c.toLowerCase() + "_selected}", "selected=\"selected\"" );
				}else{
					html= html.replace("{wheelchair_" + c.toLowerCase() + "_selected}", "" );
				}
			}
			
		  
			html= html.replace("{elementid}", "" + this.getNode().getId());
			html= html.replace("{_name_}", "" + this.getNode().getTags().getTagValue("name"));
			html = html.replace("{function_ok_clicked}", "" + JavascriptHelper.btnOkClickedScript("/submit/railway/station",parameters));
		} catch (IOException e) {
			e.printStackTrace();
			html = e.getMessage();
		}
		return html;
	}

	public String SubmitData(int elementId,  String wheelchair_select ) {
		 
		String xmlchangeset="<osm><changeset><tag k=\"created_by\" v=\"AO2SM 0.0.1\"/>";
		xmlchangeset += "<tag k=\"comment\" v=\"Updating some accessibility information into busstop\"/>";
		xmlchangeset += "</changeset></osm>";
		HTTPBasicAuthFilter authfilter = new HTTPBasicAuthFilter("ao2sm", "qwerty01!");
		Client cli = new Client();
		cli.addFilter(authfilter);  
		
		WebResource res = cli.resource(ao2sm.restapi.config.OSMConfigurations.APIURL() + "changeset/create");
		String xmlChangesetId =  res.put(String.class, xmlchangeset);
		
		NodeDto n = new NodeController().GetNodeFromOSM(elementId);
		n.setUser("ao2sm");
		n.setUid("720593");
		n.setChangeset(xmlChangesetId);
		
	 
		n.getTags().setTagValue("wheelchair",wheelchair_select);
	 
		String version  = "";
		try{
			String nodeXml = new NodeController().GetNodeXML(n);
			WebResource resnode = cli.resource(ao2sm.restapi.config.OSMConfigurations.APIURL() + "node/" + n.getId());
			version =  resnode.put(String.class, nodeXml);	
		}catch(Exception ex){
			String error = ex.getMessage();
			return error;
		}
		res = cli.resource(ao2sm.restapi.config.OSMConfigurations.APIURL() + "changeset/" + xmlChangesetId + "/close");
		xmlChangesetId =  res.put(String.class, xmlchangeset);
		
		return "Dados actualizados";
	}

	public NodeDto getNode() {
		return node;
	}


	public void setNode(NodeDto node) {
		this.node = node;
	}

}
