package ao2sm.restapi.dto.accessibility.amenity;

import java.io.IOException;

import ao2sm.restapi.config.OSMConfigurations;
import ao2sm.restapi.dto.accessibility.IElementBase;
import ao2sm.restapi.helpers.JavascriptHelper;


public class parking implements IElementBase {
	private int elementid;
	private int spacesForDisabled;
	private String name;

	public int getSpacesForDisabled() {
		return spacesForDisabled;
	}

	public void setSpacesForDisabled(int spacesForDisabled) {
		this.spacesForDisabled = spacesForDisabled;
	}
	
	public int getElementid() {
		return elementid;
	}

	public void setElementid(int elementid) {
		this.elementid = elementid;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
 
	 
	@Override
	 public String ToHtmlForm(){
		String filepath = OSMConfigurations.htmlTemplatesPath() +  "amenity_parking.html";
		String html ="";
		String[] parameters = new String[2];
		parameters[0] = "spacesForDisabled";
		parameters[1] = "elementid";
		
		try {
			html = ao2sm.restapi.helpers.FileHelper.readFile(filepath);
			html= html.replace("{spacesForDisabled}", "" + this.getSpacesForDisabled());
			html= html.replace("{elementid}", "" + this.getElementid());
			html= html.replace("{_name_}", "" + this.getName());
			html = html.replace("{function_ok_clicked}", "" + JavascriptHelper.btnOkClickedScript("/submit/amenity/parking",parameters));
		} catch (IOException e) {
			e.printStackTrace();
			html = e.getMessage();
		}
		return html;
	}

	
	 

	
	
	
	
}
