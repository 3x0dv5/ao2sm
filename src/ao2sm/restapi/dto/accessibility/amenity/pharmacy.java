package ao2sm.restapi.dto.accessibility.amenity;

import java.io.IOException;

import ao2sm.restapi.config.OSMConfigurations;
import ao2sm.restapi.dto.accessibility.IElementBase;
import ao2sm.restapi.helpers.JavascriptHelper;

public class pharmacy implements IElementBase{
	private String wheelchair;
	private String name;
	private int elementid;
	
	@Override
	public String ToHtmlForm() {
		String filepath = OSMConfigurations.htmlTemplatesPath() +  "amenity_pharmacy.html";
		String html ="";
		String[] parameters = new String[5];
		parameters[0] = "elementid";
		parameters[1] = "wheelchair_no_checked";
		parameters[2] = "wheelchair_yes_checked";
		parameters[3] = "wheelchair_limited_checked";
		parameters[4] = "wheelchair_no_info_checked";
		
		
		try {
			html = ao2sm.restapi.helpers.FileHelper.readFile(filepath);
			
			
			html= html.replace("{wheelchair_no_checked}", this.getWheelchair().toLowerCase().equals("no")?"checked":"" );
			html= html.replace("{wheelchair_yes_checked}", this.getWheelchair().toLowerCase().equals("yes")?"checked":"" );
			html= html.replace("{wheelchair_limited_checked}", this.getWheelchair().toLowerCase().equals("limited")?"checked":"" );
			if(!this.getWheelchair().toLowerCase().equals("no") && !this.getWheelchair().toLowerCase().equals("yes") && !this.getWheelchair().toLowerCase().equals("limited")){
				html= html.replace("{wheelchair_no_info_checked}", "checked" );	
			}else{html= html.replace("{wheelchair_no_info_checked}", "" );}
			
			html= html.replace("{elementid}", "" + this.getElementid());
			html= html.replace("{_name_}", "" + this.getName());
			html = html.replace("{function_ok_clicked}", "" + JavascriptHelper.btnOkClickedScript("/submit/amenity/pharmacy",parameters));
		} catch (IOException e) {
			e.printStackTrace();
			html = e.getMessage();
		}
		return html;
	}


	
	public String getWheelchair() {
		return wheelchair;
	}

 
	public void setWheelchair(String wheelchair) {
		this.wheelchair = wheelchair;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public int getElementid() {
		return elementid;
	}



	public void setElementid(int id) {
		this.elementid = id;
	}
}
