package ao2sm.restapi.dto;

import java.util.ArrayList;

public class TagsDtoList extends ArrayList<TagDto>{

	 public Boolean ContainsAmenity(){
		 return ContainsKey("amenity");
	 }
	 
	 public Boolean ContainsHighway(){
		 return ContainsKey("highway");
	 }
	 
	 public Boolean ContainsKey(String key){
		 for(TagDto t:this){
			 if(t.getKey().equals(key))
				 return true;
		 }
		 return false;
	 }
	 

	public String getAmenityValue() {
		 return getTagValue("amenity");
	}
	
	public String getTagValue(String key){
		 for(TagDto t:this){
			 if(t.getKey().equals(key))
				 return t.getValue();
		 }
		 return "";
	}
	
	public void setTagValue(String key, String value){
		if(!this.ContainsKey(key)){
			TagDto t = new TagDto();
			t.setKey(key);
			t.setValue(value);
			this.add(t);
		}else{
			this.getTag(key).setValue(value);
		}
	}
	
	public TagDto getTag(String key){
		 for(TagDto t:this){
			 if(t.getKey().equals(key))
				 return t;
		 }
		 return null;
	}
	
	public int getTagValueInt(String key){
		String tVal = getTagValue(key);
		if(tVal == "")
			return 0;
		return Integer.parseInt(tVal);
	}
}
