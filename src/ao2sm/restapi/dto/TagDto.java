package ao2sm.restapi.dto;

public class TagDto {
	private String Key;
	private String Value;
	
	public TagDto(){}
	public TagDto(String k, String v){
	 setKey(k);
	 setValue(v);
	}
	
	public String getKey() {
		return Key;
	}

	public void setKey(String key) {
		Key = key;
	}

	public String getValue() {
		return Value;
	}

	public void setValue(String value) {
		Value = value;
	}
}
