package ao2sm.restapi.helpers;


public class JavascriptHelper {
	public static String btnOkClickedScript(String submitpath, String[] parameters) {
		if(parameters.length==0)
			return "";
		
		String script = "function btn_ok_clicked(){";
		script +="var datastring = ";
		
		for(int i = 0; i<parameters.length; i++){
			
			if(i==0){
				script += "'";
			}else{
				script += "+'&";
			}
			if(parameters[i].endsWith("_checked")){
				script +=  parameters[i] + "=' + document.getElementById('" + parameters[i] + "').checked";
			} 
			else{
				script +=  parameters[i] + "=' + document.getElementById('" + parameters[i] + "').value";	
			}
			
			
			
			
			if(i+1<parameters.length){
				
			}else{
				script += ";";
			}
		}
		
		script +="$.ajax({type:'POST',url:theapibaseurl+'"+ submitpath + "',";
		
		script += "data:datastring, success:function(data){" +
								 "applicationcontroller.showSuccessUpdateMessage(data);" +
				                 "applicationcontroller.getMapController().unselect_feature(); " +
				               "}";
		
		script +="});"; //fim ajax
		
		script += "};btn_ok_clicked(); return false;";
		
		return script;
	}
}

/*
function submit(){ 
	var dataString = 'spacesForDisabled='+ document.getElementById('spacesForDisabled').value;  
	alert (dataString);return false;  
	$.ajax({  
	  type: "POST",  
	  url: theapibaseurl + "/submit/amenity/parking",  
	  data: dataString,  
	  success: function() {  
	 
	      alert('data submited');
	     
	  }  
	});  
	return false;  
}*/