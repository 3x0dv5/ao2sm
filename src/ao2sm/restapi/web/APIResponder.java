package ao2sm.restapi.web;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.*;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.*;

import org.joda.time.DateTime;



import ao2sm.restapi.dto.*;
import ao2sm.restapi.dto.accessibility.IElementBase;
import ao2sm.restapi.dto.accessibility.amenity.*;
import ao2sm.restapi.dto.accessibility.highway.Crossing;
import ao2sm.restapi.dto.accessibility.highway.TrafficSignals;
import ao2sm.restapi.dto.accessibility.highway.TramStop;
import ao2sm.restapi.dto.accessibility.railway.Station;
import ao2sm.restapi.web.controllers.*;

import com.sun.jersey.api.client.*;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

@Path("/osm")
public class APIResponder {	
	
	@GET @Path("/capabilities")
	@Produces(MediaType.TEXT_HTML)
	public String CapabilitiesHtml(){
		return "capabilities"; 
	}
	
	@GET @Path("/capabilities")
	@Produces(MediaType.APPLICATION_JSON)
	public String CapabilitiesJSON(){
		
		Client cli = new Client();
		WebResource r = cli.resource(ao2sm.restapi.config.OSMConfigurations.APIURL() + "/api/capabilities");
		
		String response = r.accept("application/xml").get(String.class);
		
		return response;
	}


	@POST @Path("/changeset/{ChangesetId}")
	@Produces(MediaType.APPLICATION_JSON)
	public ChangesetDto ChangesetInfoJSON(@Context HttpServletRequest req, @PathParam("ChangesetId") Integer chagesetId){
		
		HttpSession theSession = req.getSession();
		
		ChangesetDto chg =	new ChangesetController().GetChangesetFromOSM(chagesetId);
		theSession.setAttribute("changeset", chg);
		
		return chg;
	}
	
	@POST @Path("/element/node/{nodeid}")
	@Produces(MediaType.APPLICATION_JSON)
	public NodeDto NodeFromOSMJSON(@PathParam("nodeid") int idnode){
		return new NodeController().GetNodeFromOSM(idnode);
	}
	
	
	@POST @Path("/element/way/{wayid}")
	@Produces(MediaType.APPLICATION_JSON)
	public WayDto WayFromOSMJSON(@PathParam("wayid") int wayid){
		WayDto way= new WayController().GetWayFromOSM(wayid);
		return way;
	}
	
	@GET @Path("/element/html/node/{nodeid}")
	@Produces(MediaType.TEXT_HTML)
	public String NodeHtmlInfo( @PathParam("nodeid") int idnode){
		//obter o nó apartir da variavel de sessao
		//verificar o tipo de nó e rederizar de acordo
		
		//String str = req.getContextPath();
	/*	InputStream s =  req.getServletContext().getResourceAsStream("amenity_parking.html");
		
		String t = req.getServletContext().getContextPath();
		*/
		
		//File f = new File("C:\\amenity_parking.html");
		
		NodeDto node = new NodeController().GetNodeFromOSM(idnode);
		
		IElementBase elem = node.GetElementBase();
		if(elem ==null){
			return "<i>Este tipo de elemento n�o � edit�vel.</i>";
		}
		return elem.ToHtmlForm();
		
		//return "<i>teste</i> finalizado";
	}
	
	
	@POST @Path("/submit/amenity/parking")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String submitAmenityParking(
			@FormParam("elementid") String elementid,
			@FormParam("spacesForDisabled") int spacesForDisabled,
			@Context HttpServletResponse servletResponse) throws IOException {

		return elementid + " : " + spacesForDisabled;
	}
	
	@POST @Path("/submit/amenity/pharmacy")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String submitAmenityPharmacy(
			@FormParam("elementid") int elementid,
			@FormParam("wheelchair_no_checked") boolean wheelchair_no_checked,
			@FormParam("wheelchair_yes_checked") boolean wheelchair_yes_checked,
			@FormParam("wheelchair_limited_checked") boolean wheelchair_limited_checked,
			@FormParam("wheelchair_no_info_checked") boolean wheelchair_no_info_checked,
			@Context HttpServletResponse servletResponse) throws IOException {

		 
		String xmlchangeset="<osm><changeset><tag k=\"created_by\" v=\"AO2SM 0.0.1\"/>";
		xmlchangeset += "<tag k=\"comment\" v=\"Updating some accessibility information into pharmacy\"/>";
		xmlchangeset += "</changeset></osm>";
		HTTPBasicAuthFilter authfilter = new HTTPBasicAuthFilter("ao2sm", "qwerty01!");
		Client cli = new Client();
		cli.addFilter(authfilter);  
		
		WebResource res = cli.resource(ao2sm.restapi.config.OSMConfigurations.APIURL() + "changeset/create");
		String xmlChangesetId =  res.put(String.class, xmlchangeset);
		NodeDto n = new NodeController().GetNodeFromOSM(elementid);
		n.setUser("ao2sm");
		n.setUid("720593");
		n.setChangeset(xmlChangesetId);
		TagDto t = n.getTags().getTag("wheelchair");
		if(t==null){
			t = new TagDto();
			t.setKey("wheelchair");
			n.getTags().add(t);
		}
		t.setValue((wheelchair_no_checked == false?(wheelchair_yes_checked==false?(wheelchair_limited_checked==false?"":"limited"):"yes"):"no"));
		String version  = "";
		try{
			String nodeXml = new NodeController().GetNodeXML(n);
			WebResource resnode = cli.resource(ao2sm.restapi.config.OSMConfigurations.APIURL() + "node/" + n.getId());
			version =  resnode.put(String.class, nodeXml);	
		}catch(Exception ex){
			String error = ex.getMessage();
			return error;
		}
		res = cli.resource(ao2sm.restapi.config.OSMConfigurations.APIURL() + "changeset/" + xmlChangesetId + "/close");
		xmlChangesetId =  res.put(String.class, xmlchangeset);
		
		return elementid + " : V : " + version + " " + (wheelchair_no_checked == false?(wheelchair_yes_checked==false?(wheelchair_limited_checked==false?"":"limited"):"yes"):"no");
	
	 
	}
	
	
	@POST @Path("/submit/highway/busstop")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String submitHighwayBusStop(
			@FormParam("elementid") int elementid,
			@FormParam("wheelchair_no_checked") boolean wheelchair_no_checked,
			@FormParam("wheelchair_yes_checked") boolean wheelchair_yes_checked,
			@FormParam("wheelchair_limited_checked") boolean wheelchair_limited_checked,
			@FormParam("wheelchair_no_info_checked") boolean wheelchair_no_info_checked,
			@FormParam("tactilepaving_checked") boolean tactilepaving_checked,
			@FormParam("shelter_checked") boolean shelter_checked,
			@Context HttpServletResponse servletResponse) throws IOException {

		 
		String xmlchangeset="<osm><changeset><tag k=\"created_by\" v=\"AO2SM 0.0.1\"/>";
		xmlchangeset += "<tag k=\"comment\" v=\"Updating some accessibility information into busstop\"/>";
		xmlchangeset += "</changeset></osm>";
		HTTPBasicAuthFilter authfilter = new HTTPBasicAuthFilter("ao2sm", "qwerty01!");
		Client cli = new Client();
		cli.addFilter(authfilter);  
		
		WebResource res = cli.resource(ao2sm.restapi.config.OSMConfigurations.APIURL() + "changeset/create");
		String xmlChangesetId =  res.put(String.class, xmlchangeset);
		String wheelchair =  ((wheelchair_no_checked == false?(wheelchair_yes_checked==false?(wheelchair_limited_checked==false?"":"limited"):"yes"):"no"));
		
		NodeDto n = new NodeController().GetNodeFromOSM(elementid);
		n.setUser("ao2sm");
		n.setUid("720593");
		n.setChangeset(xmlChangesetId);
		
		n.getTags().setTagValue("wheelchair",wheelchair);
		n.getTags().setTagValue("tactile_paving", tactilepaving_checked?"yes":"no");
		n.getTags().setTagValue("shelter", shelter_checked?"yes":"no");
		
		String version  = "";
		try{
			String nodeXml = new NodeController().GetNodeXML(n);
			WebResource resnode = cli.resource(ao2sm.restapi.config.OSMConfigurations.APIURL() + "node/" + n.getId());
			version =  resnode.put(String.class, nodeXml);	
		}catch(Exception ex){
			String error = ex.getMessage();
			return error;
		}
		res = cli.resource(ao2sm.restapi.config.OSMConfigurations.APIURL() + "changeset/" + xmlChangesetId + "/close");
		xmlChangesetId =  res.put(String.class, xmlchangeset);
		
		return "Dados actualizados";
	
	 
	}
	
	@POST @Path("/submit/highway/crossing")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String submitHighwayCrossing(
			@FormParam("elementid") int elementid,
			@FormParam("crossing_unmarked_checked") boolean crossing_unmarked_checked,
			@FormParam("crossing_uncontrolled_checked") boolean crossing_uncontrolled_checked,
			@FormParam("crossing_traffic_signals_checked") boolean crossing_traffic_signals_checked,
			@FormParam("crossing_traffic_signals_sound_checked") boolean crossing_traffic_signals_sound_checked,
			@FormParam("crossing_traffic_signals_vibration_checked") boolean crossing_traffic_signals_vibration_checked,
			@FormParam("crossing_ref_select") String crossing_ref_select,
			@FormParam("sloped_curb_select") String sloped_curb_select,
			@FormParam("tactile_paving_select") String tactile_paving_select,
			@Context HttpServletResponse servletResponse) throws IOException {

		 Crossing cross = new Crossing();
		 
		
		 return cross.SubmitData(elementid, crossing_unmarked_checked,crossing_uncontrolled_checked,crossing_traffic_signals_checked,
				 crossing_traffic_signals_sound_checked,crossing_traffic_signals_vibration_checked,crossing_ref_select,
				 sloped_curb_select,tactile_paving_select);
	
	 
	}
	
	@POST @Path("/submit/highway/traffic_signals")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String submitHighwayTrafficSignals(
			@FormParam("elementid") int elementid,
			@FormParam("vibration_select") String vibration_select,
			@FormParam("arrow_select") String arrow_select,
			@FormParam("minimap_select") String minimap_select,
			@FormParam("sound_select") String sound_select,
			@Context HttpServletResponse servletResponse) throws IOException {

		 TrafficSignals cross = new TrafficSignals();
		 
		
		 return cross.SubmitData(elementid, vibration_select, arrow_select, minimap_select,sound_select);
	 
	}
	 
	@POST @Path("/submit/highway/tram_stop")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String submitHighwayTramStop(
			@FormParam("elementid") int elementid,
			@FormParam("crossing_select") String crossing_select,
			@FormParam("tactile_paving_select") String tactile_paving_select,
			@FormParam("wheelchair_select") String wheelchair_select,
			@FormParam("shelter_select") String shelter_select,
			@Context HttpServletResponse servletResponse) throws IOException {

		 TramStop cross = new TramStop();
		 
		
		 return cross.SubmitData(elementid, crossing_select, tactile_paving_select, wheelchair_select,shelter_select);
	 
	}
	@POST @Path("/submit/railway/station")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String submitrailwaystation(
			@FormParam("elementid") int elementid,
			@FormParam("wheelchair_select") String wheelchair_select,
			@Context HttpServletResponse servletResponse) throws IOException {

		 Station s = new Station();
		 
		
		 return s.SubmitData(elementid,  wheelchair_select);
	 
	}
	
	/*
	@GET @Path("/map/test")
	public String Test(){
		return "TEST OK";
	}*/
	
	//@POST @Path("/map/bbox/{bbox}")
	@GET @Path("/map")
	@Produces(MediaType.APPLICATION_JSON)
	public String MapFromOSMJSON(@Context HttpServletRequest request, @Context HttpServletResponse response, @QueryParam("bbox") String bbox){
		MapRequestController controller = new MapRequestController();
		MapRequestDto  map = controller.GetMap(bbox);
		String json = map.GetGeoJSON();
		
	/*	 FileWriter stream;
		try {
			stream = new FileWriter("/home/r/workspace_gfoss/AO2SM/log.log.json");
			BufferedWriter out = new BufferedWriter(stream);
			out.write(json);
			out.close();
		} catch (IOException e) {
			 
			e.printStackTrace();
		} */
		
		
		return json;
	/*	
		
		String bbox = "";
		
		return controller.GetMap(bbox);*/
	}
	
}
