package ao2sm.restapi.web.controllers;

import ao2sm.restapi.dto.*;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.thoughtworks.xstream.XStream;

public class NodeController {

	public NodeController(){
		
	}
	
	public NodeDto GetNodeFromOSM(int id){
		//max 8400297
		Client cli = new Client();
		WebResource res = cli.resource(ao2sm.restapi.config.OSMConfigurations.APIURL() + "node/" + id);
		
		String response = "";
		try{		 
			response = res.accept("application/xml").get(String.class);
		}catch(Exception ex){ 
			NodeDto dto = new NodeDto();
			dto.setId(-1);
			return dto;
		}

		
		XStream mapper = new NodeMapper().GetMap(response);
		NodeRootDto dtoResponse = (NodeRootDto)mapper.fromXML(response);
			
		if(dtoResponse==null) 
			return NodeRootDto.NotFound().getNode();
		    
		return dtoResponse.getNode();
		
	}
	
	public String GetNodeXML(NodeDto node){
	//	String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		
		
	String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	xml +="<osm>";
			
	xml += "<node";	
	xml += " id=\""+ node.getId() + "\" ";
	xml += "lat=\""+ node.getLat() + "\" ";
	xml += "lon=\""+ node.getLon() + "\" ";
	xml += "version=\""+ node.getVersion() + "\" ";
	xml += "changeset=\""+ node.getChangeset() + "\" ";
	xml += "user=\""+ node.getUser() + "\" ";
	xml += "uid=\""+ node.getUid() + "\" ";
	xml += "visible=\""+ node.getVisible() + "\" ";
	xml += "timestamp=\""+ node.getTimestamp() + "\"";
	xml+= ">";
	
	for(TagDto t:node.getTags()){
		xml+= "<tag k=\""+ t.getKey()+ "\" v=\""+ t.getValue() +"\"/>";
	}
	
	xml +="</node>";
	
	xml += "</osm>";
	
	return xml;			
		
	}
	
}
