package ao2sm.restapi.web.controllers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import ao2sm.restapi.config.OSMConfigurations;
import ao2sm.restapi.dto.*;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.thoughtworks.xstream.XStream;

public class MapRequestController {

	public MapRequestController(){
		
	}
	
	public MapRequestDto GetMap(String bbox){
		Client cli = new Client();
		//[amenity=hospital][bbox=-6,50,2,61]
		
		WebResource res = cli.resource(ao2sm.restapi.config.OSMConfigurations.APIURL() + "map?bbox=" + bbox);
		 
		
		String response = "";
		//try{		 
			response = res.accept("application/xml").get(String.class);
	//	}catch(Exception ex){ 
			
		//}
		
		long leng = response.length();
		
		FileWriter stream;
		try {
			stream = new FileWriter("C:\\ao2sm\\osmrequest.log.xml");
			BufferedWriter out = new BufferedWriter(stream);
			out.write(response);
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//XStream mapper = new MapRequestMapper().GetMap(response);
		MapRequestDto dtoResponse = new MapRequestDto();
		try{
			dtoResponse = (MapRequestDto)new MapRequestMapper().fromXML(response);
		}catch(Exception ex){
			
		}
		return dtoResponse;
	}
}