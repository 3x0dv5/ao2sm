package ao2sm.restapi.web.controllers;

import ao2sm.restapi.dto.*;
import ao2sm.restapi.config.*;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.thoughtworks.xstream.XStream;


public class ChangesetController {
	
	public ChangesetController(){
		
	}
	
	public ChangesetDto GetChangesetFromOSM(int id){
	
		Client cli = new Client();
		WebResource res = cli.resource(OSMConfigurations.APIURL() + "changeset/" + id);
			
		String response = "";
		try{		
			response = res.accept("application/xml").get(String.class);
		}catch(Exception ex){
			return ChangesetRootDto.NotFound().getChangeset();
		}
		
		XStream mapper = new ChangesetMapper().GetMap(response); 
        ChangesetRootDto dtoResponse = (ChangesetRootDto)mapper.fromXML(response.replace("ed_", "ed__").replace("min_", "min__").replace("max_", "max__"));
			
		if(dtoResponse==null) 
			return ChangesetRootDto.NotFound().getChangeset();
		    
		return dtoResponse.getChangeset(); 	
	}
	
}
