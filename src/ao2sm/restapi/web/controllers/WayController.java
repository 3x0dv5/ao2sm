package ao2sm.restapi.web.controllers;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.thoughtworks.xstream.XStream;

import ao2sm.restapi.dto.*;

public class WayController {
	public WayDto GetWayFromOSM(int id){
		Client cli = new Client();
		WebResource res = cli.resource(ao2sm.restapi.config.OSMConfigurations.APIURL() + "way/" + id);
		
		String response = "";
		try{		
			response = res.accept("application/xml").get(String.class);
		}catch(Exception ex){
			 return WayRootDto.NotFound().getWay();
		}

		
		XStream mapper = new WayMapper().GetMap(response);
		
		WayRootDto dtoResponse = (WayRootDto)mapper.fromXML(response);
			
		if(dtoResponse==null) 
			return WayRootDto.NotFound().getWay();
		    
		return dtoResponse.getWay();
	}
}
