﻿/**
 * 
 */

	Ext.require([
 	            'Ext.window.Window',
 	            'Ext.tab.*',
 	            'Ext.toolbar.Spacer',
 	            'Ext.layout.container.Card',
 	            'Ext.layout.container.Border'
 	        ]);
 	
 	Ext.onReady(function(){
 		
 		
 		var cw;
 		applicationcontroller = new jsAO2SM.AppController();
 		
 		
 		 var item2 = Ext.create('Ext.Panel', {
             title: 'Projecto',
             html: 'O projecto "Accessibilitas" surge, no âmbito da disciplina de Geospatial Free Open Source Software do mestrado <b>C&SIG do ISEGI-UNL</b>, com o ambicioso objectivo de contribuir para um urbanismo mais inclusivo, essencial para derrubar as barreiras da mobilidade pedonal, dando lugar à rua do futuro: universal, agradável, segura e acessível no verdadeiro sentido da palavra.'
            	 + '</br><a href="http://wiki.osgeopt.pt/index.php/Accessibilitas">Mais info na wiki.osgeopt.pt...</a></br>'
            	 ,
             cls:'empty'
         });

         var item3 = Ext.create('Ext.Panel', {
             title: 'Mentores e Colaboradores',
             html:'' 
            	 
            	 +'<p> <b>Mentores Associados: </b>'
            	 + '<ul>'
            	 + '<li> Maria Valverde <a href="http://wiki.osgeopt.pt/index.php/Utilizador:MariaValverde" class="external autonumber" rel="nofollow">[MV]</a></li>'
            	 + '<li>Rui Lima <a href="http://wiki.osgeopt.pt/index.php/Utilizador:g2011148" class="external autonumber" rel="nofollow">[RL]</a></li>'
            	 + '<li> Sara Rodrigues<a href="http://wiki.osgeopt.pt/index.php/Utilizador:Sarabrodrigues" class="external autonumber" rel="nofollow">[SR]</a></li>'
            	 + '<li>Tiago Pina <a href="http://wiki.osgeopt.pt/index.php/Utilizador:Tdcpina" class="external autonumber" rel="nofollow">[TP]</a></li>'
            	 + '</ul>'
            	 +'</p></br>'
            	 +'<p> <b>Orientador:</b> Jorge Gustavo Rocha <a href="http://wiki.osgeo.org/wiki/Jorge_Gustavo_Rocha" target="_blank" >[JR]</a>.</p>'
            	 + '</br>'
            	 +'<p> <b>Agradecimento especial pelo pictograma:</b> <a href="http://www.elenadiez.es/" rel="nofollow">[Elena Díez Hernández]</a>.</p>'
            	 ,
            	 
             cls:'empty'
         });

       
         var accordion = Ext.create('Ext.Panel', {
             title: 'Accordion',
             collapsible: true,
             region:'west',
             margins:'5 0 5 5',
             split:true,
             width: 210,
             layout:'accordion',
             items: [item2, item3]
         });
         
         var item4 = Ext.create('Ext.Panel', {
             title: 'Tecnologias utilizadas',
             html: ' O código servidor do portal está assente em tecnologia JAVA. No seu ' 
                 +'desenvolvimento foram utilizadas as tecnologias:<br /> '
                 +'<a href="http://jersey.java.net/">http://jersey.java.net/</a> para o ' 
                 +'desenvolvimento da RESTAPI '
                 +'<br /> '
                 +'<a href="http://xstream.codehaus.org/">http://xstream.codehaus.org/</a> para a ' 
                 +'manipulação dos XML entregues pela API OSM<br /> '
                 +'Estes componentes Open-Source têm também eles a utilização de outros componentes ' 
                 +'Open-Source.<br /> '
                 +'<br /> '
                 +'No código cliente (browser) foi utilizado a famosa API JavaScript&nbsp; '
                 +'<a href="http://www.openlayers.org/">OpenLayers</a> para a manipulação dos dados ' 
                 +'no mapa, para o aspecto visual utilizou-se a '
                 +'<a href="http://www.sencha.com/products/extjs/">API ExtJS 4.1</a> e claro a API '
                 +'<a href="http://jquery.com/">jQuery</a><br /> '
                 +'<br /> '
                 +'<br /> '
                 +'A pesquisa de endereços usa o serviço de <i>geocoding</i> da Google'
                 +'<br /> '
                 +'<br /> '
                 +'Alguns icons são da <a href="http://www.famfamfam.com/lab/icons/silk/">FamFam</a> </br></br>'
                 +'Agradecemos desta forma a toda a comunidade pelo trabalho dedicado a todas estes ' 
                 +'componentes, sem estes não seria possível hoje em dia desenvolver algumas das  '
                 +'melhores aplicação Web da actualidade<br />', 
             cls:'empty'
         });
         var item5 = Ext.create('Ext.Panel', {
             title: 'Retribuição',
             html: 'O código está disponível para a comunidade para futuras contribuições e melhorias, em <a href="http://bitbucket.org/3x0dv5/ao2sm" >http://bitbucket.org/3x0dv5/ao2sm</a>.',
             cls:'empty'
         });
         var accordion2 = Ext.create('Ext.Panel', {
             title: 'Tecnologia',
             collapsible: true,
             region:'west',
             margins:'5 0 5 5',
             split:true,
             width: 210,
             layout:'accordion',
             items: [item4, item5]
         });
         
 		
 		
 		var westtabpanel = {
         	   	xtype:'tabpanel',
         	   	
         	   	margins: '5 5 5 0',
                tabPosition: 'top',
                 activeTab: 0,
                 
                 items:[
                        {
                        	title: 'Acerca',
                        	type:'vbox',
                        	items: [item2, item3]      	 
                        },
                 {
                     title: 'Desenvolvimento',
                     type:'vbox',
                     items:[ item5, item4]
                 },
                 {
                     title: 'A fazer...',
                     type:'vbox',
                     html:'O portal está em desenvolvimento. </br> De momento está-se a trabalhar no suporte de maior número de tags.' 
                    	 +'Está previsto para breve a integração na plataforma de uma instalação <a href="http://wiki.openstreetmap.org/wiki/Xapi#jXAPI">jXAPI</a> dedicada.'
                    	 +'Esta instalação permitirá a consulta mais rápida da informação OSM, esta limitação leva a que o mapa fique muito lento a escalas maiores (menos zoom).'
                 }]
            };
 		
 		
 		
 		Ext.util.Region.override({
 	        colors: ['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet'],
 	        nextColor: 0,
 	        show: function(){
 	            var style = {
 	                display: 'block',
 	                position: 'absolute',
 	                top: this.top + 'px',
 	                left: this.left + 'px',
 	                height: ((this.bottom - this.top) + 1) + 'px',
 	                width: ((this.right - this.left) + 1) + 'px',
 	                opacity: 0.3,
 	                'pointer-events': 'none',
 	                'z-index': 9999999
 	            };
 	            if (!this.highlightEl) {
 	                style['background-color'] = this.colors[this.nextColor];
 	                Ext.util.Region.prototype.nextColor++;
 	                this.highlightEl = Ext.getBody().createChild({
 	                    style: style
 	                });
 	                if (this.nextColor >= this.colors.length) {
 	                    this.nextColor = 0;
 	                }
 	            } else {
 	                this.highlightEl.setStyle(style);
 	            }
 	        },
 	        hide: function(){
 	            if (this.highlightEl) {
 	                this.highlightEl.setStyle({
 	                    display: 'none'
 	                });
 	            }
 	        }
 	    });
 		
 		
 		 Ext.create('Ext.Viewport', {
 	        layout: {
 	            type: 'border',
 	            padding: 2
 	        },
 	        defaults: {
 	            split: true
 	        },
 	        items: [
 	            {
	 	            region: 'north',
	 	            collapsible: false,
	 	            title: '',
	 	            split: false,
	 	            height: 85,
	 	            html: '<table style="width:100%"><tr><td style="width:20px"><img src="pictograma.jpg"/></td><td><img src="accessibilitas.png"/></td><td style="text-align:center;vertical-align:middle;padding-right:10;"><div><input type="input" id="searchbox" value="" name="s" style="width:500px; height: 31px;float:left;"><input type="submit" name="submit" id="submitSearch" value="Search" style="float:left;" /></div><div class="fclear"></div></td></tr></table>'
	 	            	
 	            },{
	 	            region: 'west',
	 	            collapsible: false,
	 	            title: '',
	 	            split: true,
	 	            width: 300,
	 	            items:[westtabpanel
	 	                   ]
	 	                   
 	            },{
	 	            region: 'center',
	 	            layout: 'border',
	 	            border: false,
	 	            items: 
	 	            [
	 	             {
	 	            	 region:'north',
	 	            	 collapsible: false,
	 	            	 split: false,
	 	            	 height: 50,
	 	            	 html: '<div id="msg" style="width:100%"/>'
	 	             },
	 	            	 {
		 	                region: 'center',
		 	                collapible: false,
		 	                html: '<div id="map" style="width:100%;height:100%;"></div>',
		 	                title: ''
		 	             }
	 	            ]
	 	        },{
	 	        	region: 'south',
	 	        	collapsible: false,
	 	        	split: false,
	 	        	height: 1,
	 	        	title: '',
	 	        	html: '<div id="mapinfo">Map Info </div>'
 	        }]
 	    });
 		 
 		 if ( $.browser.msie ) {
 			var msg = document.getElementById('msg');					
			msg.innerHTML = '<b>Pondere a instalação de outro <i>browser</i></b>';
 		}
 		 
 		$("#submitSearch").click(function(){
 			applicationcontroller.executeSearch();
 		}); 
 		$("#searchbox").keyup(function(event){
 		    if(event.keyCode == 13){
 		    	applicationcontroller.executeSearch();
 		    }
 		});
 		 
 		applicationcontroller.loadmap();
 	});//end of onReady
 	
 	
 	