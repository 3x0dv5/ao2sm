﻿var jsAO2SM =jsAO2SM || {};
var thelocation = window.location.href;

var theapibaseurl = thelocation + '/api/osm';

var theapiurl = theapibaseurl + '/map';

jsAO2SM.MapController = function(){
	this.mapEngine= new OpenLayers.Map("map");
	this.geojson_layer = null;
	this.editPopup = null;
	this.selectedFeature = null;
	this.selectControl = null;
	this.MapHtmlProvider = null;
	this.WGS84Proj = new OpenLayers.Projection("EPSG:4326");
	this.SphericalMercatorProj = new OpenLayers.Projection("EPSG:900913");
};

jsAO2SM.MapController.prototype={
		WGS84ProjGet:function(){
			return this.WGS84Proj;
		},
		SphericalMercatorProjGet:function(){
			return this.SphericalMercatorProj;
		},
		loadmap:function(){
		/*	if (Ext.isIE) {
				alert("oh man! this is not a real browser... please use Chrome! Or at least Firefox. This pseudo-browser doesn't understand the real HTML5!!!!");
			}
			*/	 
			
			var controller = this;
			this.MapHtmlProvider = new jsAO2SM.MapHtmlProvider({MapController:controller});
			
			this.mapEngine= new OpenLayers.Map ("map", {
				controls:[
							new OpenLayers.Control.Navigation(),
							new OpenLayers.Control.PanZoomBar(),
							new OpenLayers.Control.LayerSwitcher(),
							new OpenLayers.Control.Attribution()],
						maxExtent: new OpenLayers.Bounds(-20037508.34,-20037508.34,20037508.34,20037508.34),
						maxResolution: 156543.0399,
						numZoomLevels: 20,
						units: 'm',
						projection: this.SphericalMercatorProjGet(),
						displayProjection: this.WGS84ProjGet(),
					} );
			
			
			this.getMapEngine().events.register('moveend',controller, controller.map_MoveEndHandler);
			this.getMapEngine().events.register('movestart',controller, controller.map_MoveStartHandler);
			
		    this.getMapEngine().addLayer(new OpenLayers.Layer.OSM());
		    
		    var ghyb = new OpenLayers.Layer.Google(
		    	    "Google Hybrid",
		    	    {type: google.maps.MapTypeId.HYBRID, numZoomLevels: 20}
		    	    // used to be {type: G_HYBRID_MAP, numZoomLevels: 20}
		    	);
		    this.getMapEngine().addLayer(ghyb);
		    var gsat = new OpenLayers.Layer.Google(
		    	    "Google Satellite",
		    	    {type: google.maps.MapTypeId.SATELLITE, numZoomLevels: 22});
		    this.getMapEngine().addLayer(gsat);
		    
		    var defStyle = {
		    				strokeColor: "white", 
		    				strokeOpacity: "1", 
		    				strokeWidth: 3, 
		    				fillColor:"red", 
		    				cursor: "pointer"};
	        var sty = OpenLayers.Util.applyDefaults(defStyle, OpenLayers.Feature.Vector.style["default"]);
	        
	        var sm = new OpenLayers.StyleMap({
	            'default': sty,
	            'select': {strokeColor: "red", fillColor: "red"}
	        });
		 
		    
		    this.geojson_layer = new OpenLayers.Layer.Vector("Acessibilidades", {
                projection: new OpenLayers.Projection("EPSG:4326"),
                strategies: [new OpenLayers.Strategy.BBOX()],
                protocol: new OpenLayers.Protocol.HTTP({
                    url: theapiurl,
                    format: new OpenLayers.Format.GeoJSON()
                }),
                maxResolution:1.20,
                styleMap: sm
            });
		    
		    this.getMapEngine().addLayer(this.geojson_layer);
		    
		    controller.selectControl = new OpenLayers.Control.SelectFeature(controller.geojson_layer,
		    		{
		    			scope: controller,		
		    			onSelect: controller.geojsonLayer_select,
		    			onUnselect: controller.geojsonLayer_unselect,
		    			
		    		});
		    this.getMapEngine().addControl(controller.selectControl);
		    controller.selectControl.activate();
		    
		    this.getMapEngine().setCenter(
					 new OpenLayers.LonLat(-1018860.2024141,4680807.7416477),
					 17, 
					 false, 
					 true
			 );
		    
		   
	    	applicationcontroller.geolocateUser(function(position){
		    	var pos = new OpenLayers.LonLat(position.coords.longitude,position.coords.latitude);
		    	controller.getMapEngine().setCenter(
		    			pos.transform(controller.WGS84ProjGet(), controller.SphericalMercatorProjGet()),
		    			18,
		    			false,
		    			true
		    	);
		    }, function(error){
		    	alert(error);
		    		switch(error.code)  
		    		{  
	                 case error.PERMISSION_DENIED: 
	                	 alert("you don't share your position with Accessibilitas :( ");  
	                	 break;  
	   
	                 case error.POSITION_UNAVAILABLE: 
	                	 alert("could not detect current position");  
	                	 break;  
	   
	                 case error.TIMEOUT: 
	                	 alert("retrieving position timed out");  
	                	 break;  
	   
	                 default: 
	                	 alert("unknown error");  
	                 	break;  
	             }  
		    });
		    	
	 /*   	 function handle_errors(error)  
	         {  
	             switch(error.code)  
	             {  
	                 case error.PERMISSION_DENIED: alert("user did not share geolocation data");  
	                 break;  
	   
	                 case error.POSITION_UNAVAILABLE: alert("could not detect current position");  
	                 break;  
	   
	                 case error.TIMEOUT: alert("retrieving position timed out");  
	                 break;  
	   
	                 default: alert("unknown error");  
	                 break;  
	             }  
	         }  
		   */
		    		       
		},
		
		userCurrentLocation: function(position){
		
			    alert('Lat: ' + position.coords.latitude + ' ' +  
			          'Lon: ' + position.coords.longitude);  
		
		},
		
		geojsonLayer_unselect: function(feature){
			
			this.getMapEngine().removePopup(this.editPopup);                                        //remove popup from map
			this.editPopup.destroy(); 
			
			document.getElementById('panoramawrapper').style.visibility="hidden";
			
			var msg = document.getElementById('msg');					msg.innerHTML = '';
		},
		
		unselect_feature: function(){
			this.selectControl.unselect(this.selectedFeature);
		},
		
		onPopupClose: function(evt){
			var controller = applicationcontroller.getMapController();
			
			controller.selectControl.unselect(controller.selectedFeature);
		},
		
		geojsonLayer_select:function(feature){
			//alert("geojsonLayer_click");
		
			this.selectedFeature = feature;
			
			var strInfo = this.MapHtmlProvider.getHtml(feature);
			
			
			
			var controller = this;
			this.editPopup = new OpenLayers.Popup.FramedCloud("chicken", 
                    feature.geometry.getBounds().getCenterLonLat(),
                    null,
                    strInfo,
                    null, true, controller.onPopupClose);
		
			feature.popup = this.editPopup;
			this.getMapEngine().addPopup(this.editPopup);
			
			var lonlat = feature.geometry.getBounds().getCenterLonLat().transform( this.SphericalMercatorProjGet(),this.WGS84ProjGet());
			
			var testPoint = new google.maps.LatLng(lonlat.lat,lonlat.lon,true);
			 var svClient = new google.maps.StreetViewService();
			 svClient.getPanoramaByLocation(testPoint, 50,function (panoramaData, status) {
			   if (status == google.maps.StreetViewStatus.OK) {
				   var panoramaOptions = {
							  position: new google.maps.LatLng(lonlat.lat,lonlat.lon),
							  pov: {
							    heading: 34,
							    pitch: 10,
							    zoom: 1
							  }
							};
							var panorama = new  google.maps.StreetViewPanorama(document.getElementById("panorama"), panoramaOptions);
							 
					document.getElementById('panoramawrapper').style.visibility="visible";
							
			   }else{
					var msg = document.getElementById('msg');
					msg.innerHTML = 'Não há Google StreetView nesta área';
				}			   
			 });
			
			
		
		
		},
		geojsonLayer_over: function(feature){
			//alert("geojsonLayer_over");
		},
		
		addLayerVectorOSM:function(data){
			var bounds = data.bounds;
			var nodes = data.nodes;
			
			var nLength = nodes.length;
			
			for(var i = 0; i < nLength; i++){
				var node = nodes[i];
				
			}
			 
		},
		
		getMapEngine: function(){
			return this.mapEngine;
		},
		
		map_MoveStartHandler: function(e,a){
			
		},
		
		map_MoveEndHandler: function(e,a){
		 
			 var currentZoomLevel = this.getMapEngine().zoom;
			 var currentCenter = new OpenLayers.LonLat(0,0);
			 currentCenter = this.getMapEngine().center;
			 
			  var html= '<div>Current Zoom Level: ' + currentZoomLevel + '<br/>'+
 			 'Current Center:' + currentCenter.toShortString() +'<br/>' 
 			+'</div>';
			  
			 document.getElementById('mapinfo').innerHTML = html;
			 
			 var msg = document.getElementById('msg');
			 msg.innerHTML = '';
			 if(this.getMapEngine().resolution > 1.20)
				 msg.innerHTML = 'A esta escala não é possível editar, aproxime o mapa (zoom in)';
			 
		},

		addMarker:function(options){
			var markers= new OpenLayers.Layer.Markers( "Markers" );
			if(this.getMapEngine().getLayersByName("Markers").length == 0){
				this.getMapEngine().addLayer(markers);
			}else{
				markers = this.getMapEngine().getLayersByName("Markers")[0];
			}
			markers.clearMarkers();
			var size = new OpenLayers.Size(21,25);
			var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
			var icon = new OpenLayers.Icon('http://www.openlayers.org/dev/img/marker.png', size, offset);
			
			var point = options.location.transform(this.WGS84ProjGet(), this.SphericalMercatorProjGet());
			markers.addMarker(new OpenLayers.Marker(point,icon));
			this.getMapEngine().setCenter(point);
		},
		
		CLASS_NAME: "jsAO2SM.MapController"
};


jsAO2SM.MapHtmlProvider = function(options){
	this.MapController = options.MapController;
};
jsAO2SM.MapHtmlProvider.prototype={
	
		getHtml: function(feature){
			var strInfo = "ID:" + feature.data.id + "<br/>";
			
			var featureId = feature.data.id;
			jQuery.ajax({
				url:theapibaseurl + '/element/html/'+ feature.data.type +'/' + feature.data.id,
				async:false,
				success:function(data){
					
					strInfo = data;
				}
			});
			return strInfo;
		}

};

