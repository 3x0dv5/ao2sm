var jsAO2SM =jsAO2SM || {};

jsAO2SM.APIProxy = function(){
//	this.ApiURL = 'http://api06.dev.openstreetmap.org/';
	this.ApiURL =   theapibaseurl;
	this.ChangesetController = new jsAO2SM.ChangesetController({"APIProxy":this});
	this.ElementController = new jsAO2SM.ElementController({"APIProxy":this});
};

jsAO2SM.APIProxy.prototype={
		 
		getCapabilities:function(){
			var prox = this;
			jQuery.get(prox.ApiURL + "capabilities", function(data){
				alert(data);
			}); 
		},
		
		getChangeset:function(id){
			return this.ChangesetController.GetChangeset(id);
		},
		
		getNode:function(id){
			return this.ElementController.GetNode(id);
		},
		
		getWay:function(id){
			return this.ElementController.GetWay(id);
		},
		
		getCapabilities_success:function(data){
			alert(data);
		},
		
		getOSMNodes:function(forMapExtent, successcallback){
			 
			 var  toProjection= new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
		     var fromProjection    = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection
		     var bounds  = forMapExtent.transform( fromProjection, toProjection).toBBOX();
			 
			 var controller = this;
			 $.ajax({
				type: 'GET',
				url: this.ApiURL + "map",
				data: {"bbox": bounds},
				context:controller,
				success: function(data){
					if(data.id==-1) alert("Node not found");
					else 	successcallback(data);
				},
				dataType: "json"
			});
		 },
		
		getURL:function(){
			return this.ApiURL;
		},
		
		CLASS_NAME:"jsAO2SM.APIProxy",
		CLASS_VERSION: "0.0.1"
};

jsAO2SM.ElementController=function(options){
	this.APIProxy = options.APIProxy;
	
};

jsAO2SM.ElementController.prototype={

		GetNode:function(id){
			if(!isNumber(id)){
				alert("I need a node identifier number!");
				return false;
			}
		 	
			var controller = this;
			$.ajax({
				  type: 'POST',
				  url: this._getAPIURL() + "element/node/"+id,
				  context:controller,
				  success: function(data){
					  if(data.id==-1) alert("Node not found");
					  else 			  alert(data.id + ":" + data.user);
					  
					  //alert(JSON.stringify(data));
				  },
				  dataType: "json"
				});
		},
		
		
		GetWay:function(id){
			if(!isNumber(id)){
				alert("I need a way identifier number!");
				return false;
			}
		 	
			var controller = this;
			$.ajax({
				  type: 'POST',
				  url: this._getAPIURL() + "element/way/"+id,
				  context:controller,
				  success: function(data){
					  if(data.id==-1) alert("Way not found");
					  else 			  alert(data.id + ":" );//+ data.user);
					  
					  //alert(JSON.stringify(data));
				  },
				  dataType: "json"
				});
		},
		
		_getAPIURL:function(){
			return this.APIProxy.ApiURL;
		},
		CLASS_NAME:"jsAO2SM.ElementController",
		CLASS_VERSION: "0.0.1"
};

jsAO2SM.ChangesetController = function(options){
	this.APIProxy = options.APIProxy;
};


jsAO2SM.ChangesetController.prototype={
		
		GetChangeset:function(id){
		 	if(!isNumber(id)){
				alert("I Need a changeset Number!");
				return false;
			}
		 	
			var controller = this;
			$.ajax({
				  type: 'POST',
				  url: this._getAPIURL() + "changeset/"+id,
				  context:controller,
				  success: function(data){
					  if(data.id==-1) alert("Changeset not found");
					  else 			  alert(data.id + ":" + data.user);
					  
					  //alert(JSON.stringify(data));
				  },
				  dataType: "json"
				});
		},
		
		_getAPIURL:function(){
			return this.APIProxy.ApiURL;
		},
		
		CLASS_NAME:"jsAO2SM.ChangesetController",
		CLASS_VERSION: "0.0.1"
};





jsAO2SM.ChangetsetDto = function(){
	this.id=0;
	this.user="";
	this.uid = 0;
	this.createdAt = "";
	this.closedAt="";
	this.open = false;
	this.minLon = 0.0;
	this.minLat = 0.0;
	this.maxLon = 0.0;
	this.maxLat = 0.0;
};







function isNumber(n) {
	  return !isNaN(parseFloat(n)) && isFinite(n);
	}
