﻿var jsAO2SM =jsAO2SM || {};


jsAO2SM.AppController = function(){
	this.api = new jsAO2SM.APIProxy();
	this.mapcontroller = new jsAO2SM.MapController();
	this.resultswindow=null;
};

jsAO2SM.AppController.prototype={

		 loadmap:function(){
			 this.mapcontroller.loadmap();
		 },
		 
		 geolocateUser:function(callbackfunction) {
			 if (navigator.geolocation) { 
				 navigator.geolocation.getCurrentPosition(callbackfunction);	 
			 } else {
			     alert('bad luck... your browser is out-of-date or is unaware of where you are, please consider installing Chrome.')
			 }
		 },
		 
		 			
		 _getCurrentExtent : function(){
			 return this.mapcontroller.getMapEngine().getExtent();
		 },
		 
		 zoomToTestArea: function(){
			 this.getMapEngine().setCenter(
					 new OpenLayers.LonLat(-1018860.2024141,4680807.7416477),
					 16, 
					 false, 
					 true
			 );
		 },
		 
		 showSuccessUpdateMessage:function(message){
			//alert(message); 
		 },
		 
		 executeSearch: function(){
			 if(applicationcontroller.resultswindow!=null)
				 applicationcontroller.resultswindow.hide();
			 var searchBox = document.getElementById('searchbox');
			 if(searchBox.value.length == 0 )
				 return;
			 
			 var geocoder = new google.maps.Geocoder();
			 geocoder.geocode({'address': searchBox.value}, function(results,status){
				 if (status == google.maps.GeocoderStatus.OK) {
					 searchresult = results;
					 if(results.length==1){
					   var location = results[0].geometry.location;
					   var markertext = results[0].formatted_address;
					   var loc = new OpenLayers.LonLat(location.lng(),location.lat());
					   applicationcontroller.getMapController().addMarker({'location':loc, 'text': markertext});
					   
				   }else{
					  
					   var data =new Array(results.length);
					   var length = results.length;
					   for(var i = 0; i < length; i++){
						   var r = results[i];
						   data[i] = [ r.formatted_address, r.geometry.location.lng(),r.geometry.location.lat() ]
					   }
					   
					   var store = Ext.create('Ext.data.ArrayStore', {
					        fields: [
					           {name: 'endereco'},
					           {name: 'longitude',      type: 'float'},
					           {name: 'latitude',     type: 'float'}
					        ],
					        data: data
					    });
					   
					   var grid = Ext.create('Ext.grid.Panel', {
					        store: store,
					        stateful: true,
					        stateId: 'stateGrid',
					        columns: [
					            {
					                text     : 'Endereço',
					                flex     : 1,
					                sortable : true,
					                dataIndex: 'endereco'
					            },
					            {
					                text     : 'Longitude',
					                width    : 75,
					                sortable : false,
					                dataIndex: 'longitude'
					            },
					            {
					                text     : 'Latitude',
					                width    : 75,
					                sortable : false,
					                dataIndex: 'latitude'
					            },{
					                xtype: 'actioncolumn',
					                width: 50,
					                items: [{
					                    icon   : 'icons/application_go.png',  // Use a URL in the icon config
					                    tooltip: 'Ir para ',
					                    handler: function(grid, rowIndex, colIndex) {
					                        var rec = store.getAt(rowIndex);
					                        var loc = new OpenLayers.LonLat(rec.get('longitude'),rec.get('latitude'));
					 					   	applicationcontroller.getMapController().addMarker({'location':loc, 'text': markertext});
					                    }
					                }]
					            }
					        ],
					        height: 350,
					        width: 590,
					        title: '',
					        viewConfig: {
					            stripeRows: true
					        }
					    });
					   
					  applicationcontroller.resultswindow = Ext.create('widget.window', {
			                title: 'Endereços encontrados',
			                closable: true,
			                closeAction: 'hide',
			                
			                width: 600,
			                height: 350,
			                layout: 'border',
			                bodyStyle: 'padding: 5px;',
			                items: [grid ]
			            });
					  applicationcontroller.resultswindow.show();
				   }
				} else {
				  alert("Geocode was not successful for the following reason: " + status);
				}
			 });
		 },
		 
		 getOSMNodes:function(){
			 this.api.getOSMNodes(this._getCurrentExtent(), this.mapcontroller.addLayerVectorOSM);
		 },
		 
		 getMapInfo:function(){
			 var currentZoomLevel = this.getMapEngine().zoom;
			 var currentCenter = new OpenLayers.LonLat(0,0);
			 currentCenter = this.getMapEngine().center;
			 
			 var win = Ext.create("Ext.Window",
					 {
				 		title: 'Informação do mapa',
				 		width: 300,
				 		height: 200,
				 		html:'<div>Current Zoom Level: ' + currentZoomLevel + '<br/>'+
				 			 'Current Center:' + currentCenter.toShortString() +'<br/>' 
				 			+'</div>',
				 		modal: true
					 }
					 );
			 win.show();
			 
		 },
		 
		 getMapController:function(){
			 return this.mapcontroller;
		 },
		 getMapEngine: function(){
			 return this.mapcontroller.getMapEngine();
		 },
		 getAPI:function(){
			 return this.api;
		 }
};